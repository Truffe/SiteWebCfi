<br/>

<h2> Espace Candidature </h2>

</br>

<?php
	$leCandidat = null; 
	if (isset($_GET['idcandidat']) && $_GET['action'] == "edit")
	{
		$idcandidat = $_GET['idcandidat']; 
		$leCandidat = Modele::selectWhereCandidat($idcandidat);
	}
?>

<form method="post" action ="">
	<table border="0">
		<tr>
			<td> Nom: </td><td><input type="text" name="nom"
				value = "<?php if ($leCandidat!=null) echo $leCandidat['nom'];?>"></td>
		</tr>
		<tr>
			<td> Prénom: </td><td><input type="text" name="prenom"
				value = "<?php if ($leCandidat!=null) echo $leCandidat['prenom'];?>"></td>
		</tr>
		<tr>
			<td> Adresse: </td><td><input type="text" name="adresse"
				value = "<?php if ($leCandidat!=null) echo $leCandidat['adresse'];?>"></td>
		</tr>
		<tr>
			<td> Email: </td><td><input type="text" name="email"
				value = "<?php if ($leCandidat!=null) echo $leCandidat['email'];?>"></td>
		</tr>
		<tr>
			<td> Téléphone: </td><td><input type="text" name="tel"
				value = "<?php if ($leCandidat!=null) echo $leCandidat['tel'];?>"></td>
		</tr>
		<tr>
			<td> Diplôme: </td><td><input type="text" name="diplome"
				value = "<?php if ($leCandidat!=null) echo $leCandidat['diplome'];?>"></td>
		</tr>

		</table>
</form>
<br/>

<h2> Liste des Candidats </h2>
<table border="1">
	<tr>
		<td> Id du Candidat </td> <td> Nom </td> <td> Prénom </td> <td> Adresse </td> <td> Email </td> <td> Téléphone </td> <td> Diplôme </td>
		<td> Opérations </td>
	</tr>

	<?php
	if (isset($_POST['Valider']))
	{
		//appel du modele pour insérer un nouveau candidat dans la bdd
		Modele::insertCandidat($_POST); 
		echo "<br/> Insertion réussie";
	}
	if (isset($_GET['idcandidat']) && $_GET['action'] =="sup" ){
		
		$idcandidat= $_GET['idcandidat']; 

		Modele::deleteCandidat($idcandidat);
	}

	//appel du modele pour l'extraction de tous les candidats 
	$resultats = Modele::selectAllCandidat ();

	foreach ($resultats as $key => $unCandidat) {
		echo "<tr>
					<td>".$unCandidat["idcandidat"]."</td>
					<td>".$unCandidat["nom"]."</td>
					<td>".$unCandidat["prenom"]."</td>
					<td>".$unCandidat["adresse"]."</td>
					<td>".$unCandidat["email"]."</td>
					<td>".$unCandidat["tel"]."</td>
					<td>".$unCandidat["diplome"]."</td>
					<td>
					<a href='index.php?page=3&action=sup&idcandidat=".$unCandidat["idcandidat"]."'> Supprimer </a>
					<a href='index.php?page=3&action=edit&idcandidat=".$unCandidat["idcandidat"]."'> Editer </a>
					</td>
				</tr>";
		}
		?>

</table>
