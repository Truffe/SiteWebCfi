<br/>

<h2> Ajout d'une porte ouverte</h2>
</br>
<?php
	$laPorte = null; 
	if (isset($_GET['idporte']) && $_GET['action'] == "edit")
	{
		$idporte = $_GET['idporte']; 
		$laPorte = Modele::selectWherePorte($idporte);
	}
?>

<form method="post" action ="">
	<table border="0">
		<tr> 
			<td> Désignation : </td> <td> <input type="text" name="designation" 
				value ="<?php if ($laPorte!=null) echo $laPorte['designation']; ?>"> </td>
		</tr>

		<tr>
			<td>Date de l'évènement :  </td> <td> <input type="date" name="dateporte"
				value ="<?php if ($laPorte!=null) echo $laPorte['dateporte']; ?>"> </td>
		</tr>

		<tr> 
			<td> Heure Début : </td> <td> <input type="time" name="heuredebut"
				value ="<?php if ($laPorte!=null) echo $laPorte['heuredebut']; ?>"> </td>
		</tr>

		<tr> 
			<td> Heure Fin : </td> <td> <input type="time" name="heurefin"
				value ="<?php if ($laPorte!=null) echo $laPorte['heurefin']; ?>"> </td>
		</tr>

		<tr> 
			<td> Lieu : </td> <td> <input type="text" name="lieu"
				value ="<?php if ($laPorte!=null) echo $laPorte['lieu']; ?>"> </td>
		</tr>

		<?php 
			if ($laPorte!=null) echo "<input type='hidden' name='idporte'
			value = '".$laPorte['idporte']."'>";
		?>
		<tr> 
			<td> 
				<input type="reset" name="Annuler" value="Annuler">
			</td> 
			<td> 
				<input type="submit" 
				<?php 
				if ($laPorte !=null) echo 'name="Modifier" value="Modifier">'; 
				else echo 'name="Valider" value="Valider">'; 
				?>
			</td>
		</tr>
	</table>
</form>
<br/>
<h2> Liste des portes ouvertes </h2>
<table border="1">
	<tr>
		<td> Id De la porte</td><td> Désignation </td> <td> Date de l'évènement</td>
		<td> Heure de début </td> <td> Heure de fin </td> <td> Lieu</td>
		<td> Opérations </td>
	</tr>

	<?php
	if (isset($_POST['Modifier']))
	{
		//appel du modele pour insérer une nouvelle porte dans la bdd
		Modele::updatePorte($_POST); 
		echo "<br/> Modification réussie";
	}
	
	if (isset($_POST['Valider']))
	{
		//appel du modele pour insérer une nouvelle porte dans la bdd
		Modele::insertPorte($_POST); 
		echo "<br/> Insertion réussie";
	}
	if (isset($_GET['idporte']) && $_GET['action'] =="sup" ){
		
		$idporte= $_GET['idporte']; 

		Modele::deletePorte($idporte);
	}

	

	//appel du modele pour l'extraction de toutes les portes 
	$resultats = Modele::selectAllPortes (); 

	foreach ($resultats as  $unePorte) {
		echo "<tr> 
					<td> ".$unePorte["idporte"]."</td>
					<td> ".$unePorte["designation"]."</td>
					<td> ".$unePorte["dateporte"]."</td>
					<td> ".$unePorte["heuredebut"]."</td>
					<td> ".$unePorte["heurefin"]."</td>
					<td> ".$unePorte["lieu"]."</td>
					<td> 
					<a href='index.php?page=2&action=sup&idporte=".$unePorte["idporte"]."'> Supprimer </a>

					<a href='index.php?page=2&action=edit&idporte=".$unePorte["idporte"]."'> Editer </a>
					</td>
			</tr>";
	}
	?>
</table>
