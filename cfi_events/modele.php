<?php
	class Modele
	{
		private static $pdo ; //php data object : varibale pour se connecter à la BDD
		public static function connexion ()
		{
			try{
			Modele::$pdo = new PDO ("mysql:host=localhost;dbname=cfievents","root",""); 
			}
			catch(PDOException $exp)
			{
				echo "Erreur de connexion à la base de données ";
			}
		}
		public static function selectAllPortes ()
		{
			$requete = "select * from porte "; 
			Modele::connexion () ; 
			$select = Modele::$pdo->prepare ($requete); 
			$select->execute (); 
			$resultats = $select->fetchAll(); 
			return $resultats; 
		}

		public static function selectAllCandidat ()
		{
			$requete = "select * from candidat "; 
			Modele::connexion () ; 
			$select = Modele::$pdo->prepare ($requete); 
			$select->execute (); 
			$resultats = $select->fetchAll(); 
			return $resultats; 
		}


		public static function  insertPorte ($tab)
		{
			$requete = "insert into porte values (null, :designation, 
			:dateporte, :heuredebut, :heurefin, :lieu);"; 

			$donnees = array (":designation"=> $tab['designation'], 
					":dateporte"=> $tab['dateporte'], 
					":heuredebut"=> $tab['heuredebut'], 
					":heurefin"=> $tab['heurefin'],
					":lieu"=> $tab['lieu']
				); 
			Modele::connexion () ;
			$insert = Modele::$pdo->prepare ($requete); 
			$insert->execute ($donnees);
		}

		public static function deletePorte ($idporte)
		{
			$requete = "delete from porte where idporte = :idporte; "; 
			Modele::connexion () ; 
			$donnees = array (":idporte"=> $idporte); 

			$delete = Modele::$pdo->prepare ($requete); 
			$delete->execute ($donnees);  
		}

		public static function updatePorte ($tab)
		{
			$requete = "update porte set designation = :designation, 
			dateporte = :dateporte, heuredebut= :heuredebut, heurefin = :heurefin, lieu = :lieu  where idporte = :idporte; "; 
			
			$donnees = array (":designation"=> $tab['designation'], 
					":dateporte"=> $tab['dateporte'], 
					":heuredebut"=> $tab['heuredebut'], 
					":heurefin"=> $tab['heurefin'],
					":lieu"=> $tab['lieu'], 
					":idporte"=> $tab['idporte']
				); 

			Modele::connexion () ; 
			$update = Modele::$pdo->prepare ($requete); 
			$update->execute ($donnees);  
		}

		public static function selectWherePorte ($idporte)
		{
			$requete = "select * from porte where idporte = :idporte;"; 
			$donnees = array (":idporte"=> $idporte); 

			Modele::connexion () ; 
			$select = Modele::$pdo->prepare ($requete); 
			$select->execute ($donnees); 
			$resultat = $select->fetch(); 
			return $resultat; 
		}
	}
?>
